{-# Language DeriveFunctor, TemplateHaskell, RankNTypes, DeriveFoldable,
    DeriveTraversable #-}

module Arith
( ArithF (..),
  Arith,
  natToCoAlg,
  natToAlg,
  deno,
  coAlgTree,
  evalTreeArith,
  evalTreeArith2
) where

import Data.Fix (Fix (..))
import Data.Tree
import Text.Show.Deriving
import Data.Functor.Base (TreeF (..))
import Data.Functor.Foldable
import Data.Functor.Compose
import Data.Bifunctor (first)
import Control.Comonad.Trans.Cofree (CofreeF (..))

data ArithF r = AIntF Integer
              | AAddF r r
    deriving (Show, Eq, Functor, Foldable, Traversable)
$(deriveShow1 ''ArithF)

type Arith = Fix ArithF

-- Denotational semantics
algDeno :: ArithF Integer -> Integer
algDeno (AIntF x) = x
algDeno (AAddF x y) = x + y

deno :: Arith -> Integer
deno = cata algDeno

-- Operational semantics

-- Given an arithmetic expressions, computes all the possible
-- transitions
algTrans :: ArithF (Arith, [Arith]) -> [Arith]
algTrans (AIntF _) = []
algTrans (AAddF (Fix (AIntF n), _) (Fix (AIntF m), _)) = [Fix $ AIntF (n + m)]
algTrans (AAddF (x, xs) (y, ys)) =
    let xs' = map (Fix . flip AAddF y) xs
        ys' = map (Fix . AAddF x) ys
    in xs' ++ ys'

trans :: Arith -> [Arith]
trans = para algTrans

evalTreeArith :: Arith -> Tree Arith
evalTreeArith = evalTree trans

-- Builds the (possibly infinite) tree of all possible transition
coAlgTree :: (a -> [a]) -> a -> TreeF a a
coAlgTree t e = NodeF e (t e)

evalTree :: (a -> [a]) -> a -> Tree a
evalTree t = ana (coAlgTree t)


-- Transitions with natural transformation
natTrans :: Compose ArithF ArithF r -> Compose Maybe ArithF r
natTrans (Compose (AAddF (AIntF n) (AIntF m))) = Compose $ Just (AIntF (n + m))
natTrans _ = Compose Nothing

trans2 :: Arith -> [Arith]
trans2 = transition $ scanCoAlg natTrans

evalTreeArith2 :: Arith -> Tree Arith
evalTreeArith2 = evalTree trans2

-- Natural transformation plumbing
type Alg f a = f a -> a
type CoAlg f a = a -> f a
type NatTrans f g = forall r . f r -> g r
type Transition a = a -> [a]

natToCoAlg :: NatTrans f g
           -> CoAlg g (Fix f)
natToCoAlg nt fx = nt (unFix fx)

natToAlg :: NatTrans f g
         -> Alg f (Fix g)
natToAlg nt fx = Fix $ nt fx

scanCoAlg :: (Functor f)
          => NatTrans (Compose f f) g
          -> CoAlg (CofreeF f (g (Fix f))) (Fix f) 
scanCoAlg nt x = 
    let fx = unFix x
        ffx = fmap unFix fx
        g = nt (Compose ffx)
    in g :< fx

-- Can be generalize to monoid instead of list
-- Can be generalize comonad instead of cofree
collect :: (Foldable f)
        => Alg (CofreeF f (Maybe a)) [a]
collect (x :< f) = 
    let xs = maybe [] (:[]) x
    in foldl (++) xs f

transition :: (Functor f, Foldable f)
           => CoAlg (CofreeF f (Compose Maybe f (Fix f))) (Fix f) 
           -> Transition (Fix f)
transition coAlg = hylo collect (first (fmap Fix . getCompose) . coAlg)