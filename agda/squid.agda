module squid where

open import Data.Nat using (ℕ; zero; suc; _+_; _*_)
open import Data.Bool using (Bool; true; false)

data τ : Set
data σ : Set

data τ where
  nvar : ℕ → τ
  dvar : ℕ → τ
  lam : τ → τ
  app : τ → τ → τ
  susp : τ → σ → τ

data σ where
  shift : ℕ → σ
  cons : τ → σ → σ

bar : ℕ → Bool
bar _ = true

foo : ℕ → Bool
foo n = bar zero
