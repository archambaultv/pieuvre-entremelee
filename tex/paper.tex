%% For double-blind review submission, w/o CCS and ACM Reference (max submission space)
%\documentclass[acmsmall,review,anonymous]{acmart}\settopmatter{printfolios=true,printccs=false,printacmref=false}
%% For double-blind review submission, w/ CCS and ACM Reference
\documentclass[acmsmall,review,anonymous]{acmart}\settopmatter{printfolios=true}
%% For single-blind review submission, w/o CCS and ACM Reference (max submission space)
%\documentclass[acmsmall,review]{acmart}\settopmatter{printfolios=true,printccs=false,printacmref=false}
%% For single-blind review submission, w/ CCS and ACM Reference
%\documentclass[acmsmall,review]{acmart}\settopmatter{printfolios=true}
%% For final camera-ready submission, w/ required CCS and ACM Reference
%\documentclass[acmsmall]{acmart}\settopmatter{}


%% Journal information
%% Supplied to authors by publisher for camera-ready submission;
%% use defaults for review submission.
\acmJournal{PACMPL}
\acmVolume{1}
\acmNumber{POPL} % CONF = POPL or ICFP or OOPSLA
\acmArticle{1}
\acmYear{2022}
\acmMonth{1}
\acmDOI{} % \acmDOI{10.1145/nnnnnnn.nnnnnnn}
\startPage{1}

%% Copyright information
%% Supplied to authors (based on authors' rights management selection;
%% see authors.acm.org) by publisher for camera-ready submission;
%% use 'none' for review submission.
\setcopyright{none}
%\setcopyright{acmcopyright}
%\setcopyright{acmlicensed}
%\setcopyright{rightsretained}
%\copyrightyear{2018}           %% If different from \acmYear

%% Bibliography style
\bibliographystyle{ACM-Reference-Format}
%% Citation style
%% Note: author/year citations are required for papers published as an
%% issue of PACMPL.
\citestyle{acmauthoryear}   %% For author/year citations


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Note: Authors migrating a paper from PACMPL format to traditional
%% SIGPLAN proceedings format must update the '\documentclass' and
%% topmatter commands above; see 'acmart-sigplanproc-template.tex'.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Some recommended packages.
\usepackage{booktabs}   %% For formal tables:
                        %% http://ctan.org/pkg/booktabs
\usepackage{subcaption} %% For complex figures with subfigures/subcaptions
                        %% http://ctan.org/pkg/subcaption

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{mathtools}
\usepackage{amsfonts}
%% \usepackage{amssymb}
\usepackage{amsthm}
%\usepackage[hidelinks]{hyperref}
\usepackage{stackengine}
\usepackage{graphicx}
\usepackage{enumitem}

\usepackage{tikz-qtree}

\usepackage{listings}

\DeclareUnicodeCharacter{03C3}{\ensuremath{{\color{black}{\sigma}}}}
\DeclareUnicodeCharacter{207F}{\ensuremath{{\color{black}{^n}}}}
\DeclareUnicodeCharacter{2081}{\ensuremath{{\color{black}{_1}}}}
\DeclareUnicodeCharacter{2082}{\ensuremath{{\color{black}{_2}}}}
\DeclareUnicodeCharacter{2218}{\ensuremath{{\color{black}{\circ}}}}

\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}

\usepackage[textsize=scriptsize]{todonotes}

\author{Vincent Archambault}
\email{vincent.archambault-bouffard@umontreal.ca}
\author{Stefan Monnier}
\email{monnier@iro.umontreal.ca}
\affiliation{%
  \institution{DIRO - Université de Montréal}
  \city{Montréal}
  \state{Canada}
}

\usepackage{fancyvrb}
\DefineVerbatimEnvironment{code}{Verbatim}{fontsize=\small}

\title{Programming Language Semantics with Natural Transformations}


\usepackage{float}
\newfloat{example}{thp}{loe}
\floatname{example}{Example}

% Formatting commands
\newcommand \OR {~|~}


\begin{document}

\begin{abstract}
  Next time you define a programming language, use a natural transformation.
\end{abstract}

\maketitle

\section{Introduction}
In this paper, we make formal the relationship between various way to define
programming language semantics. We show how to derive denotational semantics,
operational semantics and big step semantics from a single common definition.
\todo[inline]{Could we do contextual semantic and abstract machines as well ?}
Furthermore, we explain how to also derive an explicit substitution calculus
from this definition.

The key concept is that we will use a functor for defining the syntax of the
programming language and a natural transformation to define the behavior of the
language. \cite{turi1997towards} first explored this idea from the point of view
of category theory. Our paper on the other hand explores this idea without using
any advance notion of category theory thus making it accessible to a different
audience. \cite{hutton1998fold} also did a similar connection between
denotational semantics and operational semantics, but the key concept of natural
transformation is missing. In fact we could have simply mentionned polymorphic
functions instead of natural transformations, as 

Our main contribution is to provide an automatic derivation of the various
semantics enumerated above from this natural transformation, showing they are
actually all the same idea expressed in different ways. Unlike
\cite{turi1997towards}, our approach also works with languages with binders like
the lambda calculus. We also explore how to use the idea of language semantics
through natural transformation in the context of defining an explicit
substitution calculus.

Taking inspiration from \cite{hutton1998fold}, we first use the language of
integers and addition to illustrate our purposes. We then scale our idea to the
lambda calculus and more generally languages with binders.

\section{Arithmetic expressions}
We begin by defining the language of simple arithmetic expressions built up from
the set $\mathbb{Z}$ of integers and the addition operator.
\begin{gather}
  \label{eq:lang_arith}
  E ::= \mathbb{Z} \OR {E_1} + {E_2}
\end{gather}

We can also express this grammar in Haskell by using the built-in type Integer
and the following datatype:
\begin{code}
  data Expr = Val Integer | Add Expr Expr
\end{code}

\section{Lessons from Antoine de Saint-Exupéry}
Antoine de Saint-Exupéry, widely known for his book The Little Prince,
mentionned in his book Wind, Sand and Stars that ``Perfection is achieved, not
when there is nothing more to add, but when there is nothing left to take
away''. We follow this principal in this section by first defining a simple
denotational evaluator for the arithmetic language and then taking away
everything that is not necessary for giving a precise meaning to this
language. We the proceed to do the same with operational (small-step)
semantics. At the end, we will see that both simplification leads to a final one
that reunites denotational and operational semantics.

\subsection{A simple denotational evaluator}\label{sec:simple_eval}
A simple solution to give a meaning to the grammer \ref{eq:lang_arith} is by
defining an evaluator or giving its denotational semantics:
\begin{code}
  eval :: Expr -> Integer
  eval (Val n) = n
  eval (Add x y) = eval x + eval y
\end{code}

This works, but our first observation is that both the recursion scheme and the
actual work to be done are speciefied in one function. The recusion pattern here
is that of catamorphism. So we could start by separating both concerns. In
order to do this, we need to revisit the definition of Expr to use a functor.

\begin{code}
  data ExprF r = Val Integer | Add r r
    deriving (Functor)

  data Fix f = Fix {unfix :: f (Fix f)}

  type Expr = Fix ExprF
  
  cata :: (f a -> a) -> Fix f -> a
  cata alg = go where go = alg . (fmap go) . unFix
  
  eval :: Expr -> Integer
  eval = cata alg

  alg :: ExprF Int -> Int
  alg (Val n) = n
  alg (Add x y) = x + y
\end{code}

The function alg, for algebra, is very similar to eval of section
\ref{sec:simple_eval}. But this time it is not recursive. Since catamorphism are
well known and understood, we simply need to understand the function alg to have
the semantics of the language.

So far we have defined our language using catamorphism, and this is basically
denotational semantics as shown in \cite{hutton1998fold}. Can we abstract
something else ? Surely we cannot abstract anything from the cata function and
eval. What about the algebra function alg ? It seems that we cannot simplify any
further. So let us focus on how to give meaning to the arithemetic programming
language with the dual of catamorphism, anamorphism and operational semantics.

\subsection{A simple operational evaluator}\label{sec:simple_oper}
If we are given a mathematical expression like $(1 + 2) + (3 + 4)$ to simplify,
we know we can first simplify to the left to get $3 + (3 + 4)$ or to the right
to get $(1 + 2) + 7$. Now this choice is absent from the semantics defined in
\ref{sec:simple_eval}. But we can recover this notion of choosing with branch to
evaluate first by using operational semantics.

Often expressed as transition relations on paper and in program like Coq or
Agda, we can also define the transition as a function that accepts an expression
an returns all possible expressions.
\begin{code}
  trans :: Expr -> [Expr]
  trans (Val n) = []
  trans (Add (Val n) (Val n)) = [Val (n + m)]
  trans (Add x y) = [Add x' y | <- trans x] ++ [Add x y' <- trans x]
\end{code}

Based on this transition fonction, we can generate the tree of all possible
evaluations.
\begin{code}
  data Tree a :: Node a [Tree a]  

  evalTree :: Expr -> Tree Expr
  evalTree e = Node e [evalTree e' | e' <- trans e]
\end{code}
Of course this tree will quickly become enormous and in a real implementation we
would have a way of choising which branch we want to follow and disregards the
others. Here Haskell lazy evaluation will save us from actually computing the
whole tree if it is not strictly necessary.

Just like we did in section \ref{sec:simple_eval}, let us try to separate the
recursion pattern of trans and evalTree and the code that actually gives us the
semantics of the arithmetic language. 

For the trans function, we could first simplify by expressing the list
comprehension directly so we could see what kind of recurssion patterns we need.
\begin{code}
  trans :: Expr -> Expr []
  trans (Val n) = []
  trans (Add (Val n) (Val n)) = [Val (n + m)]
  trans (Add x y) =
    let xs  = trans x
        xs' = map (flip Add y) xs
        ys  = trans y
        ys' = map (Add x) ys'
    in xs' ++ ys'
\end{code}
We can see that we need to recurse on the left and on the right for the Add
constructor and that the final value does not depend only on the recusive call,
but also on the original x and y value. This suggests the use of a paramorphism,
since both the original and recursive value are needed.
\begin{code}
  trans :: Expr -> [Expr]
  trans = para alg2

  alg2 :: ExprF (Expr, [Expr]) -> [Expr]
  alg2 (Val n) = []
  alg2 (Add (Val n, _) (Val m, _)) = [Val (n + m)]
  alg2 (Add (x, xs) (y, ys)) = 
    let xs' = map (flip Add y) xs
        ys' = map (Add x) ys'
    in xs' ++ ys'
\end{code}
So again the semantics is given by an algebra. Let us now translate evalTree
into a recursion scheme. This one is simply an anamorphism.
\begin{code}
  evalTree :: Expr -> Tree Expr
  evalTree e = ana coAlg

  coAlg :: Expr -> TreeF Expr [Expr]
  coAlg e = Node e (trans e)
\end{code}

\subsection{Can we simplify even further ?}
In both sections \ref{sec:simple_eval} and \ref{sec:simple_oper} we separated
the recursive scheme from the actual work to be done. In both cases we got out
of this process an algebra. Functions alg and alg2 are very similar. We copy the
code below so we can compare them side by side.
\begin{code}
  alg :: ExprF Int -> Int
  alg (Val n) = n
  alg (Add x y) = x + y

  alg2 :: ExprF (Expr, [Expr]) -> [Expr]
  alg2 (Val n) = []
  alg2 (Add (Val n, _) (Val m, _)) = [Val (n + m)]
  alg2 (Add (x, xs) (y, ys)) = 
    let xs' = map (flip Add y) xs
        ys' = map (Add x) ys'
    in xs' ++ ys'
\end{code}

In both algebra, we have the first line that says in their own way that any
\verb+Val n+ value is final, nothing to be done here. They also both say that we
encounter a construct like \verb+Add (Val n) (Val m)+, we can merge the two
values using standard addition. The function alg2 on the other hand goes on to
say a bit more. It specifies that if we don't find an addition of two integers,
we can keep looking into the first parameter or into the seconde, but not both
at the same time.

The fact that alg2 tells more than alg indicates that there is something left to
be removed. Could there be a function that captures the first two lines of alg
and alg2 ? The answer is yes !
\begin{code}
  h :: ExprF (ExprF r) -> Maybe Int
  h (Val n) = Just n
  h (Add (Val n) (Val m)) = Just (n + m)
  h _ = Nothing

  h1 :: ExprF r -> Maybe Int
  h1 (Val n) = Just n
  h1 _ = Nothing

  -- Technically we  want the type, 2 levels but no leafs
  h2 :: ExprF (ExprF r) -> Maybe Int
  h2 (Val n) = h1 (Val n)
  h2 (Add (Val n) (Val m)) = + <$> h1 (Val n) <*> h1 (Val m)
  h2 _ = Nothing

  
\end{code}

This functions specifies the only essential thing needed to know about the
language, that as we said, a construct like \verb+Add (Val n) (Val m)+ can be
reduced using standard addition. Now how can we recover the denotational
semantics and the operational semantics from this ?

The denotational semantics is based on a catamorphism. Assuming the expression
is finite in size, then we must reach the leafs in order to start computing the
catamorphism. In other words this means that if we set the parameter r in h to
False, we must recover a value. Indeed
\begin{code}
  h :: ExprF (ExprF False) -> Maybe Int
  <=> By definition of ExprF and exfalso
  h :: ExprF (Val Int) -> Maybe Int
  <=> By exfalso, unwrapping Just
  h :: ExprF (Val n) -> Int
  <=> By adding the Val constructor so we have an algebra
  h :: ExprF (Val n) -> Val n
\end{code}

\section{Conclusion}
\label{conclusion}
Good bye now. Have a nice day !

\bibliography{refs}

\end{document}

%%% Local Variables:
%%% TeX-master: "paper"
%%% End:
